package com.hbloc.sale.database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaleDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaleDatabaseApplication.class, args);
    }

}
