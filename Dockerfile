# Use a base image with Java and Gradle installed
FROM gradle:8.7-jdk17 AS builder

# Set the working directory in the container
WORKDIR /app

# Copy the Gradle build files first (these rarely change, so they are good for caching)
COPY build.gradle .
COPY settings.gradle .

# Download the dependencies
RUN gradle dependencies

# Now copy the rest of the application files (these might change more often)
COPY src ./src

# Copy the application.properties file separately
COPY src/main/resources/application.properties ./src/main/resources/application.properties

# Build the application
RUN gradle bootJar


FROM openjdk:17 AS runtime
WORKDIR /app
# Copy the generated JAR file to the container
COPY --from=builder /app/build/libs/*.jar /app/app.jar

# Specify the entry point for the container
ENTRYPOINT ["java", "-jar", "./app.jar"]
